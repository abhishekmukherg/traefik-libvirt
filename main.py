import asyncio
import concurrent
import contextlib
import functools
import ipaddress
from os import name
from typing import List, Mapping, NamedTuple, Optional
from xml.etree import ElementTree
import logging

import libvirt
import uvicorn
from fastapi import FastAPI
from pydantic import BaseSettings, BaseModel, Field
from starlette.responses import JSONResponse


LOG = logging.getLogger(__name__)
XMLNS = "http://traefik.home.mukherg.com/"
NAMESPACES = {"traefik": XMLNS}


class Settings(BaseSettings):
    qemu_name: Optional[str] = None


settings = Settings()
app = FastAPI()


class TraefikRouterTLS(BaseModel):
    certresolver: str


class TraefikRouter(BaseModel):
    # entry_points: List[str] = Field(alias="entryPoints")
    middlewares: List[str]
    service: str
    rule: str
    tls: TraefikRouterTLS


class TraefikServiceServer(BaseModel):
    url: str


class TraefikServiceLoadBalancer(BaseModel):
    servers: List[TraefikServiceServer]


class TraefikHttpService(BaseModel):
    load_balancer: TraefikServiceLoadBalancer = Field(alias="loadBalancer")


class TraefikHttp(BaseModel):
    routers: Mapping[str, TraefikRouter]
    services: Mapping[str, TraefikHttpService]


class TraefikFileProvider(BaseModel):
    http: TraefikHttp


@app.get("/", response_model=TraefikFileProvider)
async def read_root() -> TraefikFileProvider:
    async with LibvirtConnection(settings.qemu_name) as conn:
        domain_ids = await conn.list_domains_id()
        if domain_ids is None:
            return JSONResponse(status_code=504, content={"error": "null domain_ids"})
        for domain_id in domain_ids:
            domain = await conn.lookup_by_id(domain_id)
            try:
                address = await domain.interface_address(0)
            except libvirt.libvirtError:
                continue
            metadata = await domain.metadata()
            address = next(iter(address.values()))
            # TODO: Multiple VM
            return TraefikFileProvider(http=TraefikHttp(
                routers={metadata.hostname: TraefikRouter(
                    middlewares=["hsts-header@file"],
                    tls=TraefikRouterTLS(certresolver="letsencrypt"),
                    service=metadata.hostname,
                    rule=f"Host(`{metadata.hostname}.home.mukherg.com`)",
                )},
                services={metadata.hostname: TraefikHttpService(
                    loadBalancer=TraefikServiceLoadBalancer(
                        servers=[TraefikServiceServer(
                            url="http://{}:{}/".format(address.addresses[0].ip, metadata.port)
                        )]
                    )
                )},
            ))
    return []


class LibvirtConnection(contextlib.AbstractAsyncContextManager["LibvirtConnection"]):
    def __init__(
        self,
        qemu_name: Optional[str],
        *,
        loop: asyncio.AbstractEventLoop = None,
        executor: Optional[concurrent.futures.Executor] = None
    ):
        self.qemu_name = qemu_name
        self.loop = loop or asyncio.get_running_loop()
        self.executor = executor
        self.conn = None

    async def __aenter__(self):
        self.conn = await self.loop.run_in_executor(
            self.executor, libvirt.openReadOnly, self.qemu_name
        )
        return self

    async def __aexit__(self, exc_type, exc_value, traceback):
        await self.loop.run_in_executor(self.executor, self.conn.close)

    async def list_domains_id(self) -> List[str]:
        return await self.loop.run_in_executor(self.executor, self.conn.listDomainsID)

    async def lookup_by_id(self, id) -> "LibvirtDomain":
        dom = await self.loop.run_in_executor(self.executor, self.conn.lookupByID, id)
        if dom is None:
            return None
        return LibvirtDomain(dom, loop=self.loop, executor=self.executor)


class Interface(NamedTuple):
    addresses: List[ipaddress.IPv4Interface]
    hardware_address: str


class Metadata(NamedTuple):
    enable: bool
    port: int
    hostname: str


class LibvirtDomain:
    def __init__(self, dom, *, loop=None, executor=None):
        self.dom = dom
        self.loop = loop or asyncio.get_running_loop()
        self.executor = executor

    @functools.cached_property
    def _dom_xml(self) -> ElementTree.Element:
        raw_xml = self.dom.XMLDesc(0)
        return ElementTree.fromstring(raw_xml)

    async def metadata(self) -> Optional[Metadata]:
        xml: ElementTree.Element = await self.loop.run_in_executor(
            self.executor, getattr, self, "_dom_xml"
        )
        metadata_xml = xml.find("metadata/traefik:traefik", namespaces=NAMESPACES)
        if metadata_xml is None:
            return None
        print(
            "Got traefik:enable",
            metadata_xml.findtext("traefik:enable", namespaces=NAMESPACES),
        )
        enable = (
            metadata_xml.findtext("traefik:enable", namespaces=NAMESPACES) == "true"
        )
        if not enable:
            return None
        port = (
            metadata_xml.findtext("traefik:port", namespaces=NAMESPACES)
        )
        hostname = (
            metadata_xml.findtext("traefik:hostname", namespaces=NAMESPACES)
        )
        return Metadata(enable=enable, port=int(port), hostname=hostname)

    async def interface_address(self, index) -> Mapping[str, Interface]:
        interfaces = await self.loop.run_in_executor(
            self.executor, self.dom.interfaceAddresses, index
        )
        result = {}
        for key, value in interfaces.items():
            result[key] = Interface(
                addresses=[
                    ipaddress.ip_interface((v["addr"], int(v["prefix"])))
                    for v in value["addrs"]
                ],
                hardware_address=value["hwaddr"],
            )
        return result


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
