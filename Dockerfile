FROM python:3.10.0-bullseye AS base

RUN apt-get update \
    && apt-get install -y libvirt0 \
    && pip install -U --no-cache pip setuptools wheel \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app/

FROM base AS installer
RUN apt-get update \
    && apt-get install -y libvirt-dev \
    && rm -rf /var/lib/apt/lists/*
RUN pip install --user --no-cache poetry

COPY poetry.lock pyproject.toml /app/
RUN python3 -m venv /opt/venv \
    && . /opt/venv/bin/activate \
    && ~/.local/bin/poetry install --no-dev

FROM installer AS dev
RUN . /opt/venv/bin/activate \
    && ~/.local/bin/poetry install

COPY main.py /app/main.py

EXPOSE 8000
ENTRYPOINT ["/opt/venv/bin/uvicorn", "main:app"]
CMD ["--host", "0.0.0.0"]

FROM base AS prod
COPY --from=installer /opt/ /opt/
COPY main.py /app/main.py
EXPOSE 8000
ENTRYPOINT ["/opt/venv/bin/uvicorn", "main:app"]
CMD ["--host", "0.0.0.0"]

# -v /var/run/libvirt/libvirt-sock:/var/run/libvirt/libvirt-sock
